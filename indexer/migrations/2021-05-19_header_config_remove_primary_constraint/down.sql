DROP TABLE header_config;
CREATE TABLE header_config (
	teehistorian_id INTEGER NOT NULL,
	config_id INTEGER NOT NULL,
	FOREIGN KEY(config_id) REFERENCES config(config_id),
	FOREIGN KEY(teehistorian_id) REFERENCES header(teehistorian_id),
	PRIMARY KEY(config_id, teehistorian_id)
);