DROP TABLE header_config;
CREATE TABLE header_config (
	header_config_id INTEGER PRIMARY KEY NOT NULL,
	teehistorian_id INTEGER NOT NULL,
	config_id INTEGER NOT NULL,
	FOREIGN KEY(config_id) REFERENCES config(config_id),
	FOREIGN KEY(teehistorian_id) REFERENCES header(teehistorian_id)
);