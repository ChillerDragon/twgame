#!/usr/bin/env python3

import hashlib # sha256
import binascii # crc32
import subprocess
import ast
import os

GIT='/usr/bin/git'

import unicodedata
import re

def slugify(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalized('NFKC', value)
    else:
        value = unicodedata.normalized('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value)
    return re.sub(r'[-\s]+', '-', value).strip('-_')

def get_checksums(bytes):
    sha256 = hashlib.sha256(bytes).hexdigest()
    crc = "{:08x}".format(binascii.crc32(bytes))
    return sha256, crc

def get_checksums_file(filename):
    with open(filename, 'rb') as f:
        bytes = f.read()
        return get_checksums(bytes)

def get_bytes_object(git_dir, object):
    process = subprocess.run([GIT, '-C', git_dir, 'cat-file', '-p', object], capture_output=True, check=True)
    return process.stdout

def get_commits(git_dir, commit_range):
    process = subprocess.run([GIT, '-C', git_dir, 'log', '--pretty=format:%H', commit_range], capture_output=True, check=True)
    return process.stdout.decode()

def process_commit_new_maps(git_dir, commit, out_dir):
    process = subprocess.run([GIT, '-C', git_dir, 'diff-tree', '--diff-filter=AM', '--no-renames', '-r', commit], capture_output=True, check=True)
    files = process.stdout.decode().split('\n')
    for file in files[1:-1]:
        object, filename = file.split('\t')
        if filename.startswith('"'):
            filename = ast.literal_eval(filename)
        object = object.split(' ')[3]
        if filename.endswith('.map'):
            bytes = get_bytes_object(git_dir, object)
            sha256, crc = get_checksums(bytes)
            filename = filename.split('/')[-1] # remove the path
            filename = '_'.join([slugify(filename[:-4]), sha256]) + '.map'
            out = os.path.join(out_dir, filename)
            if os.path.isfile(out):
                print('duplicate', out)
            else:
                print(filename)
                with open(out, "wb") as f:
                    f.write(bytes)

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--commit-range', default='*', help='commit range to scan for maps')
    parser.add_argument('--ddnet-maps', default='../ddnet-maps', help='ddnet-maps repository to scan the maps from')
    parser.add_argument('--output', default='maps/', help='Directory to put the found maps into')
    args = parser.parse_args()

    os.makedirs(args.output, exist_ok=True)
    for commit in get_commits(args.ddnet_maps, args.commit_range).split("\n"):
        process_commit_new_maps(args.ddnet_maps, commit, args.output)


if __name__ == '__main__':
    main()


