use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::sync::mpsc;
use teehistorian::{Th, ThBufReader};

use self::diesel::prelude::*;
use crate::model::{NewHeader, NewHeaderConfig, NewMeta, ThHeader, UpdateMeta};

use clap::Parser;
use diesel::sql_query;
use diesel_migrations::{EmbeddedMigrations, MigrationHarness};
use rayon::prelude::*;
use std::path::Path;

#[macro_use]
extern crate diesel;

mod model;
mod schema;

pub fn establish_connection(path: &str) -> SqliteConnection {
    let mut conn = SqliteConnection::establish(path)
        .unwrap_or_else(|_| panic!("Error connecting to {}", path));
    sql_query("PRAGMA foreign_keys = ON")
        .execute(&mut conn)
        .unwrap();
    sql_query("PRAGMA synchronous = OFF")
        .execute(&mut conn)
        .unwrap();
    conn
}

fn insert_header(
    conn: &mut SqliteConnection,
    header: &NewHeader,
    config_ids: &[i32],
) -> Result<i32, String> {
    use crate::schema::header::dsl;
    diesel::insert_or_ignore_into(schema::header::table)
        .values(header)
        .execute(conn)
        .map_err(|err| err.to_string())?;

    // retrieve teehistorian_id
    let results = dsl::header
        .select(dsl::teehistorian_id)
        .filter(dsl::game_uuid.eq(header.game_uuid))
        .load::<i32>(conn)
        .map_err(|err| err.to_string())?;
    let teehistorian_id = *results.first().unwrap();

    // store configs
    for c_id in config_ids {
        diesel::insert_or_ignore_into(schema::header_config::table)
            .values(NewHeaderConfig {
                teehistorian_id,
                config_id: *c_id,
            })
            .execute(conn)
            .map_err(|err| err.to_string())?;
    }

    Ok(teehistorian_id)
}

fn current_time() -> String {
    chrono::Utc::now().to_rfc3339_opts(chrono::SecondsFormat::Secs, true)
}

fn insert_meta(conn: &mut SqliteConnection, th_meta: NewMeta) -> Result<i32, String> {
    use crate::schema::meta::dsl;
    diesel::insert_into(schema::meta::table)
        .values(&th_meta)
        .execute(conn)
        .map_err(|err| err.to_string())?;
    let results = dsl::meta
        .select(dsl::meta_id)
        .filter(dsl::file_name.eq(th_meta.file_name))
        .load::<i32>(conn)
        .map_err(|err| err.to_string())?;
    Ok(results.into_iter().next().unwrap())
}

fn update_meta(
    conn: &mut SqliteConnection,
    meta_id: i32,
    th_meta: UpdateMeta,
) -> Result<(), String> {
    use crate::schema::meta::dsl;
    let updated_rows = diesel::update(schema::meta::table)
        .filter(dsl::meta_id.eq(meta_id))
        .set(th_meta)
        .execute(conn)
        .map_err(|err| err.to_string())?;
    debug_assert_eq!(updated_rows, 1);
    Ok(())
}

fn update_meta_file_attributes(
    conn: &mut SqliteConnection,
    meta_id: i32,
    file_size: i64,
    file_modified: &str,
    parse_time: &str,
) -> Result<(), String> {
    use crate::schema::meta::dsl;
    let updated_rows = diesel::update(schema::meta::table)
        .filter(dsl::meta_id.eq(meta_id))
        .set((
            dsl::parse_time.eq(parse_time),
            dsl::file_size.eq(file_size),
            dsl::file_modified.eq(file_modified),
        ))
        .execute(conn)
        .map_err(|err| err.to_string())?;
    debug_assert_eq!(updated_rows, 1);
    Ok(())
}

#[derive(Debug, PartialEq, Eq)]
pub struct Stats {
    bytes_added: i64,
    change: Change,
}

impl Stats {
    pub fn new(bytes_added: i64, change: Change) -> Stats {
        Stats {
            bytes_added,
            change,
        }
    }
}

#[derive(Debug, PartialEq, Eq)]
pub enum Change {
    Ignored,   // file same size, therefore didn't change
    Updated,   // th file with already parsed header got bigger
    Unchanged, // didn't add a header
    NewBroken, // New file with broken header
    Added,     // New file with parseable header
    Fixed,     // previously broken file now with a parseable header
}

fn handle_teehistorian_file(
    current_files: &HashMap<String, (i32, Option<i32>)>,
    filename: &str,
) -> Result<SendHeader, String> {
    // get new file attributes and check if we want to (re)parse the header
    let file_metadata = std::fs::metadata(filename).map_err(|err| err.to_string())?;
    let file_size = file_metadata.len() as i64;
    let file_modified = file_metadata.modified().map_err(|err| err.to_string())?;
    let file_modified = chrono::DateTime::<chrono::Utc>::from(file_modified)
        .to_rfc3339_opts(chrono::SecondsFormat::Secs, true);

    let meta = ThMeta {
        parse_time: current_time(),
        file_name: filename.to_owned(),
        file_size,
        file_modified,
    };

    let meta_id = if let Some((meta_id, teehistorian_id)) = current_files.get(filename) {
        if teehistorian_id.is_some() {
            // only update the file size
            return Ok(SendHeader {
                meta_id: Some(*meta_id),
                meta,
                th_header: None,
            });
        }
        Some(*meta_id)
    } else {
        None
    };

    // parse the header
    let f = File::open(filename).map_err(|err| err.to_string())?;
    let th_header = match Th::parse(ThBufReader::new(f)) {
        Ok(mut th_reader) => {
            // it is a teehistorian file, now try to extract the header
            match th_reader.header() {
                Ok(header) => {
                    // now try to parse the header
                    let header_size = header.len() as i32;
                    match serde_json::from_slice::<ThHeader>(header) {
                        Ok(th_header) => Some((th_header, header_size)),
                        Err(err) => {
                            println!("{} Error parsing header with serde: {}", filename, err);
                            None
                        }
                    }
                }
                Err(teehistorian::Error::IoError(err)) => return Err(err.to_string()),
                Err(err) => {
                    // non-parseable header
                    println!("{} Error parsing header: {}", filename, err);
                    None
                }
            }
        }
        Err(teehistorian::Error::IoError(err)) => return Err(err.to_string()),
        Err(err) => {
            // non-parseable header
            println!("{} Error parsing header: {}", filename, err);
            None
        }
    };

    Ok(SendHeader {
        meta_id,
        meta,
        th_header,
    })
}

#[derive(Debug, Parser)]
#[command(name = "twgame", about = "An teehistorian indexer and analyzer")]
struct Opt {
    #[arg(default_value = ".")]
    dir: String,

    #[arg(default_value = "teehistorian.sqlite")]
    teehistorian_db: String,
}

fn is_teehistorian(entry: &Path) -> bool {
    matches!(entry.extension(), Some(e) if e.to_string_lossy().to_lowercase() == "teehistorian")
}

#[derive(Default, Debug)]
struct AggStatistics {
    bytes_added: i64,
    num_ignored: i32,    // file same size, therefore didn't change
    num_updated: i32,    // th file with already parsed header got bigger
    num_unchanged: i32,  // Already broken th file still broken
    num_new_broken: i32, // New file with broken header
    num_added: i32,      // New file with parseable header
    num_fixed: i32,      // previously broken file now with a parseable header
}

impl std::ops::AddAssign<Stats> for AggStatistics {
    fn add_assign(&mut self, rhs: Stats) {
        self.bytes_added += rhs.bytes_added;
        match rhs.change {
            Change::Ignored => self.num_ignored += 1,
            Change::Updated => self.num_updated += 1,
            Change::Unchanged => self.num_unchanged += 1,
            Change::NewBroken => self.num_new_broken += 1,
            Change::Added => self.num_added += 1,
            Change::Fixed => self.num_fixed += 1,
        }
    }
}

impl std::ops::AddAssign<AggStatistics> for AggStatistics {
    fn add_assign(&mut self, rhs: AggStatistics) {
        self.bytes_added += rhs.bytes_added;
        self.num_ignored += rhs.num_ignored;
        self.num_updated += rhs.num_updated;
        self.num_unchanged += rhs.num_unchanged;
        self.num_new_broken += rhs.num_new_broken;
        self.num_added += rhs.num_added;
        self.num_fixed += rhs.num_fixed;
    }
}

pub const MIGRATIONS: EmbeddedMigrations = diesel_migrations::embed_migrations!("migrations/");

fn get_ignored_files(conn: &mut SqliteConnection, days: i32) -> HashSet<String> {
    let files = diesel::sql_query(
        "SELECT file_name FROM meta WHERE JULIANDAY(parse_time) - JULIANDAY(file_modified) > ?",
    )
    .bind::<diesel::sql_types::Integer, _>(days)
    .load::<model::MetaFileName>(conn)
    .unwrap();
    files.into_iter().map(|s| s.file_name).collect()
}

fn get_existing(conn: &mut SqliteConnection, days: i32) -> HashMap<String, (i32, Option<i32>)> {
    let files = diesel::sql_query("SELECT file_name, meta_id, teehistorian_id FROM meta WHERE JULIANDAY(parse_time) - JULIANDAY(file_modified) <= ?")
        .bind::<diesel::sql_types::Integer, _>(days)
        .load::<model::MetaFileNameIds>(conn)
        .unwrap();
    files
        .into_iter()
        .map(|s| (s.file_name, (s.meta_id, s.teehistorian_id)))
        .collect()
}

struct ThMeta {
    parse_time: String,
    file_name: String,
    file_size: i64,
    file_modified: String,
}

struct SendHeader {
    meta_id: Option<i32>, // update existing meta entry
    meta: ThMeta,
    /// header and header_size
    th_header: Option<(ThHeader, i32)>,
}

fn add_teehistorian_file(conn: &mut SqliteConnection, new: &SendHeader) -> Result<Stats, String> {
    let teehistorian_id = if let Some(th_header) = &new.th_header {
        match NewHeader::from_th_header(conn, th_header.1, &th_header.0) {
            Ok((th_header, config_ids)) => {
                // get teehistorian id
                Some(insert_header(conn, &th_header, &config_ids)?)
            }
            Err(err) => {
                println!(
                    "{} Error parsing header into struct: {}",
                    new.meta.file_name, err
                );
                None
            }
        }
    } else {
        None
    };

    let change = match (new.meta_id.is_some(), teehistorian_id.is_some()) {
        (true, true) => Change::Updated,
        (true, false) => Change::Unchanged,
        (false, true) => Change::Added,
        (false, false) => Change::NewBroken,
    };

    // update meta entry if exists otherwise insert new meta entry
    if let Some(meta_id) = new.meta_id {
        if let Some(teehistorian_id) = teehistorian_id {
            update_meta(
                conn,
                meta_id,
                UpdateMeta {
                    teehistorian_id,
                    parse_time: &new.meta.parse_time,
                    file_size: new.meta.file_size,
                    file_modified: &new.meta.file_modified,
                },
            )?;
        } else {
            update_meta_file_attributes(
                conn,
                meta_id,
                new.meta.file_size,
                &new.meta.file_modified,
                &new.meta.parse_time,
            )?;
        }
    } else {
        insert_meta(
            conn,
            NewMeta {
                teehistorian_id,
                parse_time: &new.meta.parse_time,
                file_name: &new.meta.file_name,
                file_size: new.meta.file_size,
                file_modified: &new.meta.file_modified,
            },
        )?;
    };
    Ok(Stats::new(new.meta.file_size, change))
}

fn process_th_files(mut conn: SqliteConnection, rx: mpsc::Receiver<SendHeader>) {
    let start_time = chrono::Utc::now();
    println!("0 ms: start processing files");
    let mut agg_stats = AggStatistics::default();
    loop {
        // bundle 4096 insertions into one transaction
        let result = conn
            .immediate_transaction::<_, diesel::result::Error, _>(|inner_conn| {
                let mut agg_stats = AggStatistics::default();
                loop {
                    match rx.recv() {
                        Ok(th_header) => match add_teehistorian_file(inner_conn, &th_header) {
                            Ok(stats) => {
                                if stats.change == Change::NewBroken {
                                    println!(
                                        "{} Added with broken header",
                                        th_header.meta.file_name
                                    );
                                }
                                agg_stats += stats;
                            }
                            Err(err) => {
                                println!(
                                    "{} Error inserting into db: {}",
                                    th_header.meta.file_name, err
                                );
                            }
                        },
                        Err(_) => {
                            // sender closed channel
                            return Ok((false, agg_stats));
                        }
                    }
                    if agg_stats.num_fixed + agg_stats.num_added > 4095 {
                        return Ok((true, agg_stats));
                    }
                }
            })
            .map_err(|err| err.to_string());

        match result {
            Ok((cont, agg_stat)) => {
                agg_stats += agg_stat;
                println!(
                    "{} ms processed {:?}",
                    chrono::Utc::now()
                        .signed_duration_since(start_time)
                        .num_milliseconds(),
                    agg_stats
                );
                if !cont {
                    break;
                }
            }
            Err(err) => {
                println!("Error in transaction: {}", err);
                break;
            }
        }
    }
    println!(
        "{} ms total processed {:?}",
        chrono::Utc::now()
            .signed_duration_since(start_time)
            .num_milliseconds(),
        agg_stats
    );
}

fn main() {
    // preprocess all files to ignore
    let (tx, rx) = mpsc::sync_channel(65536);
    let start_time = chrono::Utc::now();
    let opt = Opt::parse();
    let mut conn = establish_connection(&opt.teehistorian_db);
    conn.run_pending_migrations(MIGRATIONS)
        .expect("could not create tables");

    let ignore_files = get_ignored_files(&mut conn, 7);
    println!(
        "{} ms loaded ignore files",
        chrono::Utc::now()
            .signed_duration_since(start_time)
            .num_milliseconds()
    );
    let existing = get_existing(&mut conn, 7);
    println!(
        "{} ms loaded existing files",
        chrono::Utc::now()
            .signed_duration_since(start_time)
            .num_milliseconds()
    );

    let process_thread = std::thread::spawn(|| process_th_files(conn, rx));

    rayon::ThreadPoolBuilder::new()
        .num_threads(num_cpus::get() * 2)
        .build_global()
        .unwrap();

    walkdir::WalkDir::new(opt.dir)
        .into_iter()
        .par_bridge()
        .filter_map(|e| match e {
            Ok(f) => Some(f),
            Err(err) => {
                println!("Error directory {}", err);
                None
            }
        })
        .filter(|e| !e.path().is_dir() && is_teehistorian(e.path()))
        .filter_map(
            |file| match file.into_path().into_os_string().into_string() {
                Ok(f) => Some(f),
                Err(err) => {
                    println!("Convert to utf-8 error in file {:?}", err);
                    None
                }
            },
        )
        .filter(|file| !ignore_files.contains(file))
        .for_each_with(tx, |tx, file| {
            match handle_teehistorian_file(&existing, &file) {
                Ok(send_header) => tx.send(send_header).unwrap(),
                Err(err) => println!("{} Error: {}", file, err),
            }
        });

    process_thread.join().unwrap();
    println!(
        "{} ms total time finished",
        chrono::Utc::now()
            .signed_duration_since(start_time)
            .num_milliseconds()
    );
}
