use clap::Parser;
use std::fs::File;
use teehistorian::{Error, Th, ThBufReader};

/// Parses teehistorian file and prints containing teehistorian header and messages to standard output.
///
/// You can find documentation of all Teehistorian messages at https://ddnet.org/docs/libtw2/teehistorian/
#[derive(Debug, Parser)]
#[command(author, version, about)]
struct Opt {
    /// Path to teehistorian file to output
    teehistorian_file: String,
}

fn main() {
    let opt = Opt::parse();
    let f = File::open(opt.teehistorian_file).unwrap();

    let mut th = Th::parse(ThBufReader::new(f)).unwrap();
    println!("{}", String::from_utf8_lossy(th.header().unwrap()));

    loop {
        match th.next_chunk() {
            Ok(chunk) => println!("{:?}", chunk),
            Err(Error::Eof) => break,
            Err(err) => {
                println!("Err: {}", err);
                break;
            }
        }
    }
}
