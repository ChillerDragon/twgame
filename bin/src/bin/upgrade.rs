use clap::Parser;
use std::fs::File;
use std::io::BufWriter;
use std::path::Path;
use teehistorian::chunks::Join;
use teehistorian::{Chunk, Error, Th, ThBufReader, ThWriter};

/// Modifies teehistorian file and adds Join-Message at the beginning. Was used for stronghold
/// test case. This tool is not necessary anymore, as the validator can handle missing Join messages
/// at the beginning.
///
/// This script was written as a hotfix and shouldn't be used. The reason for this bug is
/// missing Join-messages after map changes. It is fixed in DDNet with
/// https://github.com/ddnet/ddnet/pull/6744. The validator can also parse unfixed teehistorian
/// files nowadays.
#[derive(Debug, Parser)]
#[command(author, version, about)]
struct Opt {
    /// input teehistorian file
    input: String,
    /// output teehistorian file
    output: String,
}

fn main() {
    let opt = Opt::parse();
    let inp = File::open(opt.input).unwrap();

    let opath = Path::new(&opt.output);
    let out = BufWriter::new(File::create(opath).unwrap());

    let mut th = Th::parse(ThBufReader::new(inp)).unwrap();
    let header = String::from_utf8_lossy(th.header().unwrap());
    println!("{}", header);

    let mut writer = ThWriter::new(out, &header).unwrap();
    writer.add_chunk(&Chunk::Join(Join { cid: 0 })).unwrap();
    loop {
        match th.next_chunk() {
            Ok(chunk) => {
                println!("{:?}", chunk);
                writer.add_chunk(&chunk).unwrap();
            }
            Err(Error::Eof) => break,
            Err(err) => {
                println!("Err: {}", err);
                break;
            }
        }
    }
}
