use clap::Parser;
use rayon::prelude::*;
use serde::Deserialize;
use std::fs::File;
use std::path::Path;
use teehistorian::{Chunk, Th, ThBufReader};

#[derive(Debug, Parser)]
/// Search for teehistorian which only contain the EOS-chunk. Outputs some information about these
/// Teehistorian files to stdout. Prints errors to stderr. Was written to investigate
/// https://github.com/ddnet/ddnet/issues/5121
#[command(author, version, about)]
struct Opt {
    /// directory to search for empty teehistorian files.
    #[arg(default_value = ".")]
    dir: String,
}

#[derive(Deserialize, Debug)]
pub struct ThHeader {
    pub game_uuid: String,
    pub server_version: String,
    pub start_time: String,
    pub map_name: String,
}

fn is_teehistorian(entry: &Path) -> bool {
    matches!(entry.extension(), Some(e) if e.to_string_lossy().to_lowercase() == "teehistorian")
}

fn main() {
    let opt = Opt::parse();

    rayon::ThreadPoolBuilder::new()
        .num_threads(num_cpus::get() * 2)
        .build_global()
        .unwrap();

    walkdir::WalkDir::new(opt.dir)
        .into_iter()
        .par_bridge()
        .filter_map(|e| match e {
            Ok(f) => Some(f),
            Err(err) => {
                eprintln!("Error directory {}", err);
                None
            }
        })
        .filter(|e| !e.path().is_dir() && is_teehistorian(e.path()))
        .filter_map(
            |file| match file.into_path().into_os_string().into_string() {
                Ok(f) => Some(f),
                Err(err) => {
                    eprintln!("Convert to utf-8 error in file {:?}", err);
                    None
                }
            },
        )
        .for_each(|filename| {
            let f = if let Ok(f) = File::open(&filename).map_err(|err| err.to_string()) {
                f
            } else {
                return;
            };
            match Th::parse(ThBufReader::new(f)) {
                Ok(mut th_reader) => {
                    // it is a teehistorian file, now try to extract the header
                    match th_reader.header() {
                        Ok(header) => {
                            let th_header = match serde_json::from_slice::<ThHeader>(header) {
                                Ok(th_header) => th_header,
                                Err(err) => {
                                    eprintln!(
                                        "{} Error parsing header with serde: {}",
                                        filename, err
                                    );
                                    return;
                                }
                            };

                            if matches!(th_reader.next_chunk(), Ok(Chunk::Eos)) {
                                println!(
                                    "{},{},{},{}",
                                    th_header.server_version,
                                    th_header.start_time,
                                    th_header.game_uuid,
                                    th_header.map_name
                                )
                            }
                        }
                        Err(teehistorian::Error::IoError(err)) => eprintln!("error: {err}"),
                        Err(err) => {
                            // non-parseable header
                            eprintln!("{} Error parsing header: {}", filename, err);
                        }
                    }
                }
                Err(err) => {
                    eprintln!("{} error: {}", filename, err);
                }
            }
        });
}
