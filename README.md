# TwGame

This repository contains multiple [DDNet](https://ddnet.org) related libraries and tools.
It is based on [libtw2](https://github.com/heinrich5991/libtw2), [twmap](https://gitlab.com/Patiga/TwMap) and [teehistorian](https://gitlab.com/Zwelf/teehistorian).

## Features:

Bolded items have known edge cases not yet working:

- [DDNet physics](ddnet/)
  - [x] spawn
  - [x] collision
  - [x] movement
  - [x] jump
    - [ ] jump count setter
    - [x] wall jump
    - [x] double jump refresher
  - [x] hook
    - [x] old hookthrough
    - [x] hookthrough
    - [x] directional hookthrough
  - [x] freeze
    - [ ] deep freeze
    - [ ] freeze heart
    - [ ] live freeze
  - [x] hammer
  - [x] weapon switch
  - [x] collectable weapons
    - [x] shotgun
    - [x] grenade
    - [x] laser
    - [x] ninja
  - [x] shields (removing weapons)
  - [x] solo
  - [ ] power ups
    - [ ] endless hook
    - [ ] weapons off
    - [x] collision off
    - [x] infinite jumps
    - [x] jetpack
    - [ ] hook off
  - [x] **start/finish**
  - [ ] checkpoints
  - [x] teams
    - [x] switching teams
    - [x] `/lock`
    - [x] unlock block
    - [x] join team0 on finish
  - [x] swap
  - [x] ignoring practice
  - [x] save/load
  - [ ] **chat command `/kill`**
  - [x] teleporter
    - [x] checkpoints
    - [x] hook teleporter
    - [x] weapon teleporter
  - [ ] telegun gun/grenade/laser
  - [ ] **stopper**
  - [ ] config variables
  - [x] **tune zones**
  - [ ] switch layer
    - [ ] static freezing laser
    - [ ] rotating freezing laser
    - [ ] laser length changer
    - [ ] explosion turrents
    - [ ] moving bullets
    - [ ] dragger
    - [ ] doors
  - [ ] speedup layer
  - [ ] entity speeder
  - [ ] `/spec` and `/pause`
  - [x] Spectator mode
  - [ ] ...
- [Unit tests for physics](validator/res)
- [Teehistorian replayer](validator/)
- [Teehistorian indexer](indexer) - making collections of teehistorian files searchable in sqlite3

# Future plans:

- Output teehistorian file as .demo
- Allow using DDNet C++ physics via Rust bindings
- Python binding to work with teehistorian files
- Demo reader/writer allowing conversion to and from teehistorian
- Make a server wrapper around the game implementation using libtw2 network library
- Allow rendering teehistorian files with twmap
- In teehistorian replayer allow checking which bugs quirks were abused like tele, shotgun, skippable tiles, ...
  or map specific bugs.
