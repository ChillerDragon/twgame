use crate::entities::SpawnOrder;
use crate::state::Tees;
use std::collections::HashMap;
use twsnap::uid::{PlayerUid, TeeUid};

#[derive(Debug)]
/// Entities and Tees need to access all Tees in the game world. Tees never need to access the
/// entities. This struct allows getting tees in either player_id or spawn order.
/// This struct is about caching the iterators and is the only housekeeping about available Tees
/// in the team within the accessible game state. It needs to be updated on Tee join/leave.
/// For creating snapshots it is also necessary to have a TeeUid -> PlayerUid lookup.
pub struct TeeOrder {
    /// tees in Id order TODO: use smallvec or similar to optimize heap allocation for each (mostly 1-2 tee sized) team
    id: Vec<(TeeUid, PlayerUid)>,
    /// tees in Spawn order
    spawn: Vec<(TeeUid, SpawnOrder)>,
    /// TeeUid -> PlayerUid
    player_uid: HashMap<TeeUid, PlayerUid>,
}

impl TeeOrder {
    pub(super) fn new() -> Self {
        Self {
            id: vec![],
            spawn: vec![],
            player_uid: HashMap::new(),
        }
    }
    pub(super) fn reset(&mut self) {
        self.id.clear();
        self.spawn.clear();
        self.player_uid.clear();
    }
}

impl TeeOrder {
    fn add_tee_to_all_except_spawn(&mut self, player_uid: PlayerUid, tee_uid: TeeUid) {
        let tee_legacy_id = player_uid.legacy_id();

        // do binary search of id order
        // unwrap_err, because tee mit same id shouldn't exist
        match self
            .id
            .binary_search_by(|(_, pid)| pid.legacy_id().cmp(&tee_legacy_id))
        {
            Ok(pos) => {
                println!(
                    "tee with same player_id already exists in world {}",
                    tee_legacy_id
                );
                // FIXME: panic/unwrap_err when fixed cases with two tees per player_id.
                self.id[pos] = (tee_uid, player_uid);
            }
            Err(pos) => {
                // expected situation: no tee with this player_id currently exists
                self.id.insert(pos, (tee_uid, player_uid));
            }
        };
        // unwrap: invariant: tees can't be inserted twice into Team
        assert!(self.player_uid.insert(tee_uid, player_uid).is_none());
    }

    pub(super) fn add_tee(
        &mut self,
        player_uid: PlayerUid,
        tee_uid: TeeUid,
        spawn_order: SpawnOrder,
    ) {
        // append to spawn order
        self.spawn.push((tee_uid, spawn_order));
        self.add_tee_to_all_except_spawn(player_uid, tee_uid);
    }

    pub(super) fn add_tee_from_other_team(
        &mut self,
        player_uid: PlayerUid,
        tee_uid: TeeUid,
        spawn_order: SpawnOrder,
    ) {
        // do binary search of id order
        // unwrap_err, because tee mit same exact SpawnOrder shouldn't exist
        let pos = self
            .spawn
            .binary_search_by(|(_, other_order)| other_order.cmp(&spawn_order))
            .unwrap_err();
        self.spawn.insert(pos, (tee_uid, spawn_order));
        self.add_tee_to_all_except_spawn(player_uid, tee_uid);
    }

    pub(super) fn remove_tee(&mut self, tee_uid: TeeUid) {
        self.id.retain(|(tid, _)| tid != &tee_uid);
        self.spawn.retain(|(tid, _)| tid != &tee_uid);
        self.player_uid.remove(&tee_uid);
    }

    pub(super) fn on_tee_swap(&mut self, t1: TeeUid, t2: TeeUid) {
        let pos = self
            .id
            .iter_mut()
            .position(|(tee_uid, _)| *tee_uid == t1)
            .unwrap();
        self.id
            .iter_mut()
            .find(|(tee_uid, _)| *tee_uid == t2)
            .unwrap()
            .0 = t1;
        self.id[pos].0 = t2;
        let pos = self
            .spawn
            .iter_mut()
            .position(|(tee_uid, _)| *tee_uid == t1)
            .unwrap();
        self.spawn
            .iter_mut()
            .find(|(tee_uid, _)| *tee_uid == t2)
            .unwrap()
            .0 = t1;
        self.spawn[pos].0 = t2;
    }

    pub(super) fn member_count(&self) -> usize {
        self.id.len()
    }
}

pub(crate) struct TeeOrderIter<T> {
    iter: T,
}

impl<T> Iterator for TeeOrderIter<T>
where
    T: Iterator<Item = TeeUid>,
{
    type Item = TeeUid;

    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}

impl TeeOrder {
    pub(crate) fn id_order(&self) -> TeeOrderIter<impl Iterator<Item = TeeUid> + '_> {
        TeeOrderIter {
            iter: self.id.iter().map(|(tid, _)| *tid),
        }
    }

    pub(crate) fn spawn_order(&self) -> TeeOrderIter<impl Iterator<Item = TeeUid> + '_> {
        TeeOrderIter {
            iter: self.spawn.iter().map(|(tid, _)| *tid).rev(),
        }
    }

    pub(crate) fn player_uid(&self, tee_uid: TeeUid) -> Option<PlayerUid> {
        self.player_uid.get(&tee_uid).copied()
    }
}

impl<T> TeeOrderIter<T> {
    // returns an iter over tees
    pub(crate) fn tees<U>(self, tees: &Tees<U>) -> TeesIter<T, U> {
        TeesIter { tees, iter: self }
    }

    // returns an iter over tees
    pub(crate) fn tees_mut<U>(self, tees: &mut Tees<U>) -> TeesIterMut<T, U> {
        TeesIterMut { tees, iter: self }
    }

    // returns an iter over tees
    pub(crate) fn tees_except<U>(self, tees: &Tees<U>, except: TeeUid) -> TeesExceptIter<T, U> {
        TeesExceptIter {
            except,
            tees,
            iter: self,
        }
    }

    // returns an iter over tees
    pub(crate) fn tees_except_mut<U>(
        self,
        tees: &mut Tees<U>,
        except: TeeUid,
    ) -> TeesExceptIterMut<T, U> {
        TeesExceptIterMut {
            except,
            tees,
            iter: self,
        }
    }
}

pub(crate) struct TeesIter<'a, T, U> {
    tees: &'a Tees<U>,
    iter: TeeOrderIter<T>,
}

impl<'a, T, U> Iterator for TeesIter<'a, T, U>
where
    T: Iterator<Item = TeeUid>,
{
    type Item = (TeeUid, &'a U);

    fn next(&mut self) -> Option<Self::Item> {
        if let Some(tee_uid) = self.iter.next() {
            Some((tee_uid, self.tees.get_tee(tee_uid).unwrap()))
        } else {
            None
        }
    }
}

pub(crate) struct TeesIterMut<'a, T, U> {
    tees: &'a mut Tees<U>,
    iter: TeeOrderIter<T>,
}

impl<'a, T, U> TeesIterMut<'a, T, U>
where
    T: Iterator<Item = TeeUid>,
{
    pub fn next(&mut self) -> Option<(TeeUid, &mut U)> {
        if let Some(tee_uid) = self.iter.next() {
            Some((tee_uid, self.tees.get_tee_mut(tee_uid).unwrap()))
        } else {
            None
        }
    }
}

pub(crate) struct TeesExceptIter<'a, T, U> {
    except: TeeUid,
    tees: &'a Tees<U>,
    iter: TeeOrderIter<T>,
}

impl<'a, T, U> Iterator for TeesExceptIter<'a, T, U>
where
    T: Iterator<Item = TeeUid>,
{
    type Item = (TeeUid, &'a U);

    fn next(&mut self) -> Option<Self::Item> {
        for tee_uid in self.iter.by_ref() {
            if tee_uid == self.except {
                continue;
            }
            return Some((tee_uid, self.tees.get_tee(tee_uid).unwrap()));
        }
        None
    }
}

pub(crate) struct TeesExceptIterMut<'a, T, U> {
    except: TeeUid,
    tees: &'a mut Tees<U>,
    iter: TeeOrderIter<T>,
}

impl<'a, T, U> TeesExceptIterMut<'a, T, U>
where
    T: Iterator<Item = TeeUid>,
{
    pub fn next(&mut self) -> Option<(TeeUid, &mut U)> {
        for tee_uid in self.iter.by_ref() {
            if tee_uid == self.except {
                continue;
            }
            return Some((tee_uid, self.tees.get_tee_mut(tee_uid).unwrap()));
        }
        None
    }
}
