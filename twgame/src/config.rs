use std::collections::HashMap;

/// config which can be set with explicit values
/// or if not specified returns the default value of the given version
/// can return the certainty of a value
pub struct Config {
    /// time in seconds `kill` NetMsg's (and ddrace_commands) are ignored after issuing a successful kill
    sv_kill_delay: i32,

    /// time in minutes in race until the kill NetMsg and Joining Spectators is ignored and typing
    /// `/kill` into the chat is required
    sv_kill_protection: i32,
}

impl Config {
    pub fn sv_kill_delay(&self) -> i32 {
        self.sv_kill_delay
    }
    pub fn sv_kill_protection(&self) -> i32 {
        self.sv_kill_protection
    }

    fn set_sv_kill_delay(&mut self, value: &str) {
        if let Ok(value) = value.parse() {
            if (0..=9999).contains(&value) {
                self.sv_kill_delay = value;
            }
        }
    }
    fn set_sv_kill_protection(&mut self, value: &str) {
        if let Ok(value) = value.parse() {
            if (0..=9999).contains(&value) {
                self.sv_kill_protection = value;
            }
        }
    }
}

impl Config {
    pub fn new(_version: &str) -> Config {
        Config {
            sv_kill_delay: 1,
            sv_kill_protection: 20,
        }
    }

    pub fn apply_config(&mut self, config: &HashMap<String, String>) {
        for (name, value) in config {
            match &name[..] {
                "sv_kill_delay" => self.set_sv_kill_delay(value),
                "sv_kill_protection" => self.set_sv_kill_protection(value),
                _ => todo!(),
            }
        }
    }
}
