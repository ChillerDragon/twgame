test save reload timer
test save unstarted tee

tee[0] m_Jumped (57): Some("0")
tee[0] m_JumpedTotal (58): Some("0")
tee[0] m_Jumps (59): Some("2")

solo map save/load. team_kind

* save/load on old servers
* tee non-existent during load
* one tee leaves during loading
