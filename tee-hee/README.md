# Tee-Hee

Teehistorian indexer and replayer for large amount of teehistorian files for DDNet.

Goals:

* Index all Teehistorian files
* Replay with TwGame physics engine and detect bug abuse and other anomalies
* Corrolate DDNet finish database with Teehistorian files and extract demos from finishes
